<?php

use yii\db\Migration;

/**
 * Class m180911_154717_alter_fk_comment_to_page
 */
class m180911_154717_alter_fk_comment_to_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-comment-page_id-page-id', '{{%comment}}');
        $this->addForeignKey('fk-comment-page_id-page-id', 
                '{{%comment}}', 'page_id', '{{%page}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180911_154717_alter_fk_comment_to_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180911_154717_alter_fk_comment_to_page cannot be reverted.\n";

        return false;
    }
    */
}
