<?php

use yii\db\Migration;

/**
 * Class m180910_131620_create_table_comment
 */
class m180910_131620_create_table_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'message' => $this->string(255),
        ]);
        $this->addForeignKey('fk-comment-page_id-page-id', 
                '{{%comment}}', 'page_id', '{{%page}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180910_131620_create_table_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180910_131620_create_table_comment cannot be reverted.\n";

        return false;
    }
    */
}
