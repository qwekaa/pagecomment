<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property string $title
 * @property string $body
 *
 * @property Comment[] $comments
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%page}}';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'body'], 'trim'],
            [['title', 'body'], 'filter', 'filter' => function($value){
                return \yii\helpers\HtmlPurifier::process($value);
            }],
            [['title'], 'required'],
            [['body'], 'string'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * Вывод даты создания в виде строки для отображения
     * @return string
     */
    public function getCreatedDisplayed()
    {
        return date('d.m.Y H:i:s', $this->created_at);
    }
    
    /**
     * Вывод даты обновления в виде строки для отображения
     * @return string
     */
    public function getUpdatedDisplayed()
    {
        return date('d.m.Y H:i:s', $this->updated_at);
    }
    
    /**
     * Было ли обновление страницы
     * @return boolean
     */
    public function isNotUpdated()
    {
        return $this->created_at != $this->updated_at;
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'title' => 'Заголовок',
            'body' => 'Текст',
            'createdDisplayed' => 'Создано',
            'updatedDisplayed' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['page_id' => 'id']);
    }
}
