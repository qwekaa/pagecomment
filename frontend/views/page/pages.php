<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $Pages \frontend\models\Page[] */
?>

<?php foreach ($Pages as $Page): ?>
<div class="page__block blockType" data-page_id="<?= $Page->id; ?>">
    <div class="page__title">
        <?= $Page->title; ?>
        <?= Html::tag('span', '', [
            'class' => 'editPage contentModal glyphicon glyphicon-pencil',
            'data' => [
                'action' => Url::toRoute(['page/update', 'id' => $Page->id]),
                'header' => 'Редактирование страницы №'.$Page->id,
            ],
        ]); ?>
        <?= Html::tag('span', '', [
            'class' => 'delete text-danger glyphicon glyphicon-trash',
            'data' => [
                'confirm' => 'Вы действительно желаете удалить страницу и все её комментарии?',
                'action' => Url::toRoute(['page/delete', 'id' => $Page->id]),
                'id' => $Page->id,
            ],
        ]); ?>
    </div>
    <div class="page__body">
        <p>
            <span>Создано: <?= $Page->getCreatedDisplayed(); ?></span> 
            <?php if ($Page->isNotUpdated()): ?>
                <span>Обновлено: <?= $Page->getUpdatedDisplayed(); ?></span>
            <?php endif; ?>
        </p>
        <?php if (!empty($Page->body)): ?>
            <p><?= $Page->body; ?></p>
        <?php else: ?>
            <i>Нет содержимого</i>
        <?php endif; ?>
    </div>
    <div class="page__comment">
        <?= $this->render('../comment/list', [
            'Comments' => $Page->comments
        ]); ?>
    </div>
    <div class="commentform">
        <button class="btn btn-primary btn-xs addCommentForm"
                data-page_id="<?= $Page->id; ?>"
                data-action="<?= Url::toRoute(['comment/create', 'page_id' => $Page->id]) ?>">
            Комментировать
        </button>
    </div>
    <hr />
</div>
<?php endforeach; ?>