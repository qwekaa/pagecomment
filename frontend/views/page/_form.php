<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;
/* @var $this yii\web\View */
/* @var $model frontend\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="page-form" id="pageForm">
    <?= Alert::widget([
        'options' => [
            'class' => '',
            'id' => 'responseMessage'
        ],
        'body' => '',
    ]); ?>
    <?php $form = ActiveForm::begin([
        'options' => ['data-success' => 'successPage']
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
