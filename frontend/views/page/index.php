<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $Page \frontend\models\Page */ 

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">
   
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
    <?php 
        Modal::begin([
            'header' => '',
            'toggleButton' => [
                'label' => 'Добавить страницу',
                'class' => 'btn btn-success contentModal',
                'id' => 'addPage',
                'data-action' => Url::toRoute('page/create'),
                'data-header' => 'Добавление новой страницы',
            ],
        ]);
        Modal::end();
    ?>
    </p>
    <div class="wrapPages">
        <?= $this->render('pages', ['Pages' => $Pages]); ?>
    </div>
    
</div>
