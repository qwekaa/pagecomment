<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $Comments \frontend\models\Comment[] */
if (empty($Comments)) {
    return;
}
?>

<h4>Комментарии</h4>
<?php foreach ($Comments as $Comment): ?>
<div class="comment__block blockType">
    <div class="comment__body">
        <span><?= $Comment->getCreatedDisplayed(); ?></span>
        <?= $Comment->message; ?>
        <?= Html::tag('span', '', [
            'class' => 'delete text-danger glyphicon glyphicon-trash',
            'data' => [
                'confirm' => 'Вы действительно желаете удалитькомментарий?',
                'action' => Url::toRoute(['comment/delete', 'id' => $Comment->id]),
                'id' => $Page->id,
            ],
        ]); ?>
    </div>
</div>
<?php endforeach;
