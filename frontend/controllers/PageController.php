<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Page;
use frontend\models\search\PageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            [
                'class' => 'yii\filters\AjaxFilter',
                'only' => ['create', 'update', 'delete']
            ],
        ];
    }

    /**
     * Отображение списка страни
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $Pages = $dataProvider->getModels();
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('pages', [
                'Pages' => $Pages
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'Pages' => $Pages,
        ]);
    }

    /**
     * Создание страницы
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson([
                'result' => 'success',
                'message' => 'Успешно добавлено',
                'class' => 'alert-success',
            ]);
        }
        return $this->renderPartial('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Обновление страницы
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson([
                'result' => 'success',
                'message' => 'Успешно обновлено',
                'class' => 'alert-success',
            ]);
        }
        
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Удаление страницы
     * 
     * @param integer $id
     * @return JSON
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->asJson(['result' => 'success']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
