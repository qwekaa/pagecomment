<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Comment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            [
                'class' => 'yii\filters\AjaxFilter',
                'only' => ['create', 'delete']
            ],
        ];
    }

    /**
     * Создание нового комментария
     * 
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $model = new Comment();
        $model->page_id = $page_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->asJson(['result' => 'success']);
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }


    /**
     * Удаление комментария
     * 
     * @param integer $id
     * @return mixed Возвратит JSON
     * @throws NotFoundHttpException в случае ненахождения модели Комментария
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->asJson(['result' => 'success']);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
