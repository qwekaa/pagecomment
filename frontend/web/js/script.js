jQuery(document).ready(function($){
    
    $(document).on('click', '[type="submit"]', function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        console.log(form.data('success'));
        ajaxRequest(form, form.data('success'));
        return false;
    });
    
   
    /**
     * Вызов модального окна при нажатие на редактирование страницы
     */
    $(document).on('click', '.editPage', function(e){
        $($('#addPage').data('target')).modal('show');
    });
    
    /**
     * Вызов модального окна и загрузка в него формы Страницы
     * в зависимости от значения action
     */
    $(document).on('click', '.contentModal', function(){
        $('.modal-header').text($(this).data('header'));
        $('.modal-body').load($(this).data('action'));
    });
    
    /**
     * Обработчик удаения. Нажатия на иконку корзины
     */
    $(document).on('click', '.blockType .delete:not(.process)', function(){
        if (!confirm($(this).data('confirm'))) {
            return;
        }
        var that = this;
        $(this).addClass('process');
        $.post($(this).data('action'), function(data){
            if (data.result == 'success') {
                $(that).closest('.blockType').addClass('line-through');
                setTimeout(function(){
                    $(that).closest('.blockType').remove();
                }, 1500);
            }
        });
    });
    
    /**
     * Ajax запрос на обновление данных. После успеного выполнения
     * вызов функции
     * @param {form} form
     * @param {callable} successCallable
     */
    function ajaxRequest(form, successCallable)
    {
        $.post(form.attr('action'), form.serialize(),
            function(data){
                if (typeof window[successCallable] == 'function') {
                    window[successCallable](data);
                }
                $('.wrapPages').load(window.location.href);
            }
        );
    }
    
    /**
     * Получение формы комментариев
     */
    $(document).on('click', '.addCommentForm', function(){
        $(this).closest('.commentform').load($(this).data('action'));
    });
    
});

/**
 * Функция будет вызвана после успешного Ajax запроса
 * @param {mixed} data ответ с сервера
 */
function successPage(data)
{
    if (data.result == 'success') {
        $('#responseMessage').html(data.message)
            .addClass(data.class).show();
        setTimeout(function(){
            $($('#addPage').data('target')).modal('hide');
        }, 1500);
    } else {
        $('.modal-body').html(data);
    }
}
